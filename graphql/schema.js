const { gql } = require("apollo-server")

module.exports = gql`
	type Query {
		getGenres : [Genre]
		getGenre(id: String): Genre
	}

	type Genre {
		id: ID!
		name: String!
		movie: [Movie]
	}

	type Movie {
		id: ID!
		title : String!
		genre_id: ID!
		genre: Genre
	}
`;
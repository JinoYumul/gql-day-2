const Genre = require('../models/genre')

module.exports = {
	Query: {
		getGenres(){
			return Genre.find({})
		}
	}
}
const mongoose = require('mongoose')
const { ApolloServer } = require('apollo-server')
const gqlSchema = require('./graphql/schema')
const gqlResolvers = require('./graphql/resolver')

const apolloServer = new ApolloServer({
	typeDefs: gqlSchema,
	resolvers: gqlResolvers
})

mongoose.connect('mongodb+srv://jino:jino123@cluster1.7j4kj.mongodb.net/graphql?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => {
	console.log('Now connected to local MongoDB server.')
})

apolloServer.listen().then(({ url }) => {
  console.log(`Apollo server ready at ${url}`);
});